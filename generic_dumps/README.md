# What is this folder for?

This is the root folder of all SQL dumps. When you run the dump generation process, you will get some files heere:

1. `generic_dump_allstar`: Which is a folder with all `INSERT` statements separated by table names to generate the ChEBI database.
2. `pgsql_create_tables.sql`: It is the postgres schema to re-create all tables, without data, just empty tables.
3. `pgsql_allstars.dump`: It is a dump file to re-create full schema using `pg_restore` command.