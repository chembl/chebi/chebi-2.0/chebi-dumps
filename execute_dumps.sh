#!/bin/bash

DUMPS_FOLDER=$(pwd)/generic_dumps
ALL_STAR_SUBFOLDER=${DUMPS_FOLDER}/generic_dump_allstar

# We are getting the dbt models here (i.e. the table names which will be exported).
# From the luigi task, we send the 1th parameter as follow:
# compounds;chemical_data;comments;structures;..., so basically we create an array called MODELS splitting the string by ';'.
# Based on https://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash
IFS=';' read -ra MODELS <<< "$1"

function create_postgres_schema() {
    echo "Generating schema in ${DUMPS_FOLDER}/pgsql_create_tables.sql"
    pg_dump \
        --schema-only \
        --no-owner \
        -f "${DUMPS_FOLDER}/pgsql_create_tables.sql" \
        --dbname=${PG_DBNAME} \
        --schema=chebi_allstar \
        --username=${PG_USER} \
        --host=${PG_HOST}
    echo "Schema generated successfully."
}

function create_full_postgres_dump() {
    echo "Generating dump file in ${DUMPS_FOLDER}/pgsql_allstars.dump"
    pg_dump \
        --format=custom \
        --no-owner \
        -f "${DUMPS_FOLDER}/pgsql_allstars.dump" \
        --dbname=${PG_DBNAME} \
        --schema=chebi_allstar \
        --username=${PG_USER} \
        --host=${PG_HOST}
    echo "Full dump generated successfully."
}

function create_insert_statements() {
    # Here we are creating the INSERT statements for each table.
    mkdir -p ${ALL_STAR_SUBFOLDER}
    for table_name in "${MODELS[@]}"; do
        echo "Generating dump for: ${table_name} table in ${ALL_STAR_SUBFOLDER}/${table_name}.sql"
        pg_dump \
            --data-only \
            --column-insert \
            --no-owner \
            -f "${ALL_STAR_SUBFOLDER}/${table_name}.sql" \
            --dbname=${PG_DBNAME} \
            --schema=chebi_allstar \
            --username=${PG_USER} \
            --host=${PG_HOST} \
            --table=${table_name} &&
        gzip --fast -c ${ALL_STAR_SUBFOLDER}/${table_name}.sql > ${ALL_STAR_SUBFOLDER}/${table_name}.sql.zip
        echo "Dump for ${table_name} generated successfully."
    done
}

function execute_dumps() {
    echo "========= Creating postgres dumps files for postgres =========" &&
    create_insert_statements &&
    create_postgres_schema &&
    create_full_postgres_dump
}

execute_dumps