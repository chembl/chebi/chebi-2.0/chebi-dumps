***************************************************************************
*
* Name:         INSTALL
*
* Purpose:      To describe how to import ChEBI data into an
*               PostgreSQL database.
*
* Date:         {{ release_date }}
*
* Contact:      chebi-help<AT>ebi.ac.uk
*
***************************************************************************
*
* DB Version    {{ release_version }}
*
***************************************************************************

Instructions
------------

1. Log into PostgreSQL database server where you intend to load ChEBI data
   and run the following command to create new database:

    pgdb=# create database chebi_{{ release_version }};

2. Logout of database and run the following command to load data. You will
   need to replace USERNAME, HOST and PORT with local settings.
   Depending on your database setup you may not need the host and port
   arguments.

    $> pg_restore --no-owner -h HOST -p PORT -U USERNAME -d chebi_{{ release_version }} pgsql_allstars.dump

   And that's all, now you will have ChEBI database under chebi_allstar schema in chebi_{{ release_version }} database.