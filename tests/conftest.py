import polars as pl

from pytest import fixture
from pathlib import Path

from helper_functions.files_functions import read_file

FIXTURES_PATH = Path(__file__).parent / "fixtures"


@fixture(scope="module")
def mock_compounds_with_valid_structure() -> pl.DataFrame:
    """
    This dataframe has four compounds which must exist in chebi_for_testing.owl file, their ids are:
    (65, 1230, 2674, 4492)
    :return: A dataframe with valid default structures compounds."""

    return pl.read_csv(
        source=FIXTURES_PATH / "test_compounds_with_valid_structure.tsv",
        separator="\t",
    )


@fixture(scope="module")
def mock_cross_references() -> pl.DataFrame:
    """
    This dataframe has the eight compounds, the same four mentioned in mock_compounds_with_valid_structure fixture,
    plus other four compounds which do not have a default structure, their ids are: (3008, 3992, 5686, 9368)
    :return: A dataframe with the cross-references by compound.
    """

    return pl.read_csv(source=FIXTURES_PATH / "test_cross_references.tsv", separator="\t")


@fixture(scope="module")
def expected_chebi_sdf() -> str:
    file = read_file(FIXTURES_PATH / "expected_chebi_for_testing.sdf")
    return file


@fixture(scope="module")
def expected_chebi_sdf_3_stars() -> str:
    file = read_file(FIXTURES_PATH / "expected_chebi_for_testing_3_stars.sdf")
    return file
