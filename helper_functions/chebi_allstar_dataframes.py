import polars as pl

from helper_functions.database_connection import get_pg_connection
from helper_functions.files_functions import read_file, SQL_FOLDER
from helper_functions.logger import logger


def get_molfiles() -> pl.DataFrame:
    """Get all molfiles from the database marked as a default structure."""
    logger.info("> Getting molfiles from database")
    with get_pg_connection().cursor() as cursor:
        valid_structures_sql = read_file(SQL_FOLDER / "compounds_with_valid_structure.sql")
        pg_data = pl.read_database(query=valid_structures_sql, connection=cursor)
    logger.info("> Compound with valid structures have been gotten successfully from database")

    pg_data = pg_data.select(pl.col("compound_id"), pl.col("molfile").str.strip_chars())
    return pg_data


def get_cross_references() -> pl.DataFrame:
    """Get all cross-references from the database"""
    logger.info("> Getting cross references from database")
    with get_pg_connection().cursor() as cursor:
        cross_references_sql = read_file(SQL_FOLDER / "cross_references.sql")
        pg_data = pl.read_database(cross_references_sql, connection=cursor)
    logger.info("> Cross references have been gotten successfully from database")
    return pg_data
