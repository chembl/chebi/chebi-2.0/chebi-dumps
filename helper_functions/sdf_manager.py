import pronto as pt
import itertools as it
import polars as pl
import logging
from collections.abc import Iterator

from helper_functions.ontology_functions import get_ontology, SDFVariants, AnnotationPropertyEquivalences
from helper_functions.sdf_templates import (
    ContentSDF,
    get_sdf_full_template,
    get_sdf_lite_template,
)

logger = logging.getLogger("luigi-interface")


class SDFManager:
    """
    This class generates all sdf files. We have two sdf files variants: lite and full.
    # Lite variant: It includes ChEBI ID, ChEBI Name, Star and Molfile.
    # Full variant: It includes all fields mentioned above, plus chemical information, synonyms and references.
    For each variant, we would need to generate two kind of sdf files: one to store information about 2 and 3 stars compounds,
    and another to store information about 3 stars only. To summarize, in the final of the sdf generation process, we should
    have generated:
    # Lite variant:
        - chebi_lite_3_stars.sdf
        - chebi_lite.sdf
    # Full variant:
        - chebi.sdf
        - chebi_3_stars.sdf
    In the generation process, we are going to use the ontology, so for sdf lite variant we will use chebi_lite.owl to get the
    necessary information and for sdf full varaint we will use chebi.owl.
    """

    def __init__(
        self,
        variant: SDFVariants,
        pg_valid_molfiles: pl.DataFrame,
        pg_cross_references: pl.DataFrame,
    ):
        self.variant = variant
        self.pg_valid_molfiles = pg_valid_molfiles
        self.pg_cross_references = pg_cross_references

    def get_template(self, content: ContentSDF) -> str:
        if "lite" in self.variant:
            return get_sdf_lite_template(content)
        return get_sdf_full_template(content)

    def get_ontology(self) -> tuple[Iterator[pt.Term], Iterator[pt.Term]]:
        """We are returning two ontology objects filtering compounds by stars."""
        ontology = get_ontology(self.variant)

        ontology_2_and_3_stars = (
            term
            for term in sorted(ontology.terms(), key=lambda t: int(t.id.split(":")[1]))
            if not term.obsolete and ("chebi/2:STAR" in term.subsets or "chebi/3:STAR" in term.subsets)
        )

        ontology_3_stars = (
            term
            for term in sorted(ontology.terms(), key=lambda t: int(t.id.split(":")[1]))
            if not term.obsolete and "chebi/3:STAR" in term.subsets
        )

        return ontology_2_and_3_stars, ontology_3_stars

    def get_sdf_content(self, ontology: Iterator[pt.Term]) -> Iterator[ContentSDF]:
        for compound in ontology:
            raw_id = int(compound.id.split(":")[1])
            compound_molfile = self.pg_valid_molfiles.filter(compound_id=raw_id).select("molfile")
            # Generate SDF only for compounds with a structure.
            if not compound_molfile.is_empty():
                cross_references_by_compound = self.pg_cross_references.filter(compound_id=raw_id).select(
                    ["database_name", "accession_number"]
                )
                initial_data = {
                    "id": compound.id,
                    "name": compound.name,
                    "secondary_ids": compound.alternate_ids,
                    "star": next(int(i) for i in it.chain(set(compound.subsets).pop()) if i.isdigit()),
                    "definition": compound.definition,
                    "molfile": compound_molfile.item(),
                    "iupac_names": (
                        iupac_names := [
                            synonym.description
                            for synonym in compound.synonyms
                            if synonym.type and "IUPAC" in synonym.type.description
                        ]
                    ),
                    "synonyms": set(synonym.description for synonym in compound.synonyms) - set(iupac_names),
                }
                # Chemical data: mass, monoisotopicmass, charge, smiles, formula
                annotations = {
                    AnnotationPropertyEquivalences[annotation.property.split("/")[-1]].value: int(annotation.literal)
                    if "integer" in annotation.datatype
                    else float(annotation.literal)
                    if "decimal" in annotation.datatype
                    else annotation.literal
                    for annotation in compound.annotations
                }

                # Cross references
                cross_references = {
                    source_name: accession_numbers
                    for source_name, accession_numbers in zip(
                        *cross_references_by_compound.to_dict(as_series=False).values(),
                        strict=True,
                    )
                    if not cross_references_by_compound.is_empty()
                }

                content = initial_data | annotations
                content["cross_references"] = cross_references
                yield ContentSDF(**content)

    def get_files_content(self) -> tuple[str, str]:
        ontology_2_and_3_stars, ontology_3_stars = self.get_ontology()
        template_2_and_3_stars, template_3_stars = "", ""

        for content in self.get_sdf_content(ontology_2_and_3_stars):
            template_2_and_3_stars += self.get_template(content)
        logger.info("\tContent for 2-3 stars generated")

        for content in self.get_sdf_content(ontology_3_stars):
            template_3_stars += self.get_template(content)
        logger.info("\tContent for 3 stars generated")
        return template_2_and_3_stars, template_3_stars


if __name__ == "__main__":
    pass
    # from rdkit import Chem
    # sdf_compound_errors = []
    # suppl = Chem.SDMolSupplier('../SDF/chebi.sdf', sanitize=False)
    # for mol in suppl:
    #     try:
    #         print(mol.GetPropsAsDict())
    #     except:
    #         sdf_compound_errors.append(mol)
