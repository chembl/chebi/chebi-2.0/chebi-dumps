from dataclasses import dataclass, field


@dataclass
class ContentSDF:
    name: str
    star: int
    id: str
    molfile: str
    definition: str = None
    smiles: str = None
    inchikey: str = None
    inchi: str = None
    formula: str = None
    charge: int = None
    wurcs: str = None
    mass: float = None
    monoisotopicmass: float = None
    iupac_names: list[str] = field(default_factory=list)
    secondary_ids: list[str] = field(default_factory=list)
    synonyms: list[str] = field(default_factory=list)
    cross_references: dict[str, str] = field(default_factory=dict)


def get_sdf_lite_template(content: ContentSDF) -> str:
    """Return SDF template for lite variant, space in the final is necessary, otherwise file can not be parsed by rdkit"""
    return f"""{content.molfile}

> <ChEBI ID>
{content.id}

> <ChEBI NAME>
{content.name}

> <STAR>
{content.star}

$$$$

"""


def get_sdf_full_template(content: ContentSDF) -> str:
    full_template = f"""{content.molfile}

> <ChEBI ID>
{content.id}

> <ChEBI NAME>
{content.name}

> <STAR>
{content.star}

"""
    if content.definition:
        full_template += f"""> <DEFINITION>
{content.definition}

"""
    if content.mass:
        full_template += f"""> <MASS>
{content.mass}

"""
    if content.formula:
        full_template += f"""> <FORMULA>
{content.formula}

"""
    if content.monoisotopicmass:
        full_template += f"""> <MONOISOTOPIC_MASS>
{content.monoisotopicmass}

"""
    if content.charge:
        full_template += f"""> <CHARGE>
{content.charge}

"""
    if content.wurcs:
        full_template += f"""> <WURCS>
{content.wurcs}

"""
    if content.smiles:
        full_template += f"""> <SMILES>
{content.smiles}

"""
    if content.inchi:
        full_template += f"""> <INCHI>
{content.inchi}

"""
    if content.inchikey:
        full_template += f"""> <INCHIKEY>
{content.inchikey}

"""
    if len(content.iupac_names) > 0:
        full_template += f"""> <IUPAC_NAME>
{';'.join(content.iupac_names)}

"""
    if len(content.synonyms) > 0:
        full_template += f"""> <SYNONYM>
{';'.join(content.synonyms)}

"""
    if len(content.secondary_ids) > 0:
        full_template += f"""> <SECONDARY_ID>
{';'.join(content.secondary_ids)}

"""
    # Cross references
    for source_name, accession_numbers in content.cross_references.items():
        if len(accession_numbers) > 0:
            full_template += f"""> <{source_name}>
{accession_numbers}

"""

    full_template += """$$$$

"""
    return full_template
