from typing import TypeAlias

import pronto as pt
import warnings
import envtoml
from enum import StrEnum

from helper_functions.files_functions import parent_folder
from helper_functions.logger import logger

luigi_config = envtoml.load(open(f"{parent_folder}/luigi.toml"))


class OntologyVariants(StrEnum):
    LITE = "_lite"
    CORE = "_core"
    FULL = ""
    TEST = "_for_testing"  # Used by the unit tests


SDFVariants: TypeAlias = OntologyVariants


class AnnotationPropertyEquivalences(StrEnum):
    """ChEBI ontology uses chemrof for annotation properties. We are assigning a simple value used by elasticsearch
    to create the documents.

    <value_in_chemrof> = <simple_value_used_by_elasticsearch>
    """

    generalized_empirical_formula = "formula"
    inchi_string = "inchi"
    inchi_key_string = "inchikey"
    wurcs_representation = "wurcs"
    smiles_string = "smiles"
    charge = "charge"
    mass = "mass"
    monoisotopic_mass = "monoisotopicmass"


def get_ontology(variant: OntologyVariants, gzip: bool = True) -> pt.ontology.Ontology:
    warnings.filterwarnings("ignore", category=pt.warnings.NotImplementedWarning, module="pronto")
    configuration_ontology = luigi_config.get("ontology")
    ontology_path = configuration_ontology.get("path")
    ontology_file = f"chebi{variant.value}.owl.gz" if gzip else f"chebi{variant}.owl"
    logger.info(f"> Loading {variant.name} ontology: {ontology_file}")
    path = f"{ontology_path}/{ontology_file}"
    ontology_object = pt.Ontology(path)
    logger.info(f"> {ontology_file} loaded successfully")
    return ontology_object


# ontology_subset = get_ontology(OntologyVariants.TEST, gzip=False)
