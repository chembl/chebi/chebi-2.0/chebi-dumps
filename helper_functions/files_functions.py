from pathlib import Path

parent_folder = Path(__file__).parents[1]
DUMPS_FOLDER = parent_folder / "generic_dumps"
ALL_STAR_SUBFOLDER = DUMPS_FOLDER / "generic_dump_allstar"
TEMPLATES = parent_folder / "templates"
SDF_FOLDER = parent_folder / "SDF"
SQL_FOLDER = parent_folder / "sql"
FLAT_FILES_FOLDER = parent_folder / "flat_files"


def delete_all_dumps() -> None:
    for f in DUMPS_FOLDER.iterdir():
        if f.suffix in [".sql", ".zip"]:
            f.unlink()


def delete_all_checkpoints(folder_path: Path) -> None:
    for f in folder_path.iterdir():
        if f.suffix not in [".md"]:
            f.unlink()


def read_file(path: Path, mode: str = "r") -> str:
    """A simple function to read plain files"""
    if mode not in ["r", "rb"]:
        raise ValueError("read_file function only accepts 'r' or 'rb' mode")

    if mode == "r":
        mode_file = open(path, mode=mode, encoding="utf-8")
    else:
        mode_file = open(path, mode=mode)

    with mode_file as file:
        return file.read()


def write_file(path: Path, content: str | bytes, mode: str = "w") -> None:
    if mode not in ["w", "wb"]:
        raise ValueError("write_file function only accepts 'w' or 'wb' mode")

    with open(path, mode=mode) as file:
        file.write(content)
