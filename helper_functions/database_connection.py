import envtoml
import psycopg
from helper_functions.files_functions import parent_folder

luigi_config = envtoml.load(open(f"{parent_folder}/luigi.toml"))


def get_pg_connection() -> psycopg.Connection:
    configuration_database_pg = luigi_config.get("postgres")
    user = configuration_database_pg.get("user")
    password = configuration_database_pg.get("password")
    host = configuration_database_pg.get("host")
    port = configuration_database_pg.get("port")
    db_name = configuration_database_pg.get("database")
    return psycopg.connect(dbname=db_name, user=user, password=password, host=host, port=port)
