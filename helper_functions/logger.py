import logging

# Suppress all DEBUG logs globally
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)

# Suppress DEBUG logs from all luigi loggers
logger = logging.getLogger("luigi-interface")
logger.setLevel(logging.INFO)

# Suppress DEBUG logs from luigi components
for logger_name in ["luigi", "luigi-interface", "luigi.worker", "luigi.scheduler", "luigi.task"]:
    logging.getLogger(logger_name).setLevel(logging.INFO)

# Optional: Make sure the root logger itself is set to INFO
logging.getLogger().setLevel(logging.INFO)
