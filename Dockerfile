FROM python:3.11-slim-buster
ENV PYTHONUNBUFFERED 1
ARG PG_HOST
ARG PG_PORT
ARG PG_USER
ARG PG_PASSWORD
ARG PG_DBNAME
ARG ONTOLOGY_PATH

ENV PYTHONUNBUFFERED 1
ENV PG_PORT $PG_PORT
ENV PG_USER $PG_USER
# PGPASSWORD env variable needs to mantain that special name. Take a look this stack overflow answer: https://stackoverflow.com/a/8123178/4508767
ENV PGPASSWORD $PG_PASSWORD
ENV PG_DBNAME $PG_DBNAME
ENV PG_HOST $PG_HOST
ENV ONTOLOGY_PATH $ONTOLOGY_PATH


RUN apt-get update -qq -y && \
    apt-get install -qq -y wget gnupg2 && \
    rm -rf /var/lib/apt/lists/* && \
    # Download postgres key and register it to the repo, it is necessary to install postgres-client-14
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    echo "deb https://apt-archive.postgresql.org/pub/repos/apt stretch-pgdg main" | tee  /etc/apt/sources.list.d/pgdg.list && \
    apt-get update -qq -y && \
    apt-get install -y --no-install-recommends postgresql-client-14 && \
    wget -qO /usr/local/bin/dasel https://github.com/TomWright/dasel/releases/download/v2.1.2/dasel_linux_amd64 && \
    chmod a+x /usr/local/bin/dasel

WORKDIR /chebi_dumps

COPY . /chebi_dumps

RUN pip install -r requirements.txt

RUN cp luigi.example.toml luigi.toml && \
    dasel put -f luigi.toml -t string -v '$PG_HOST' 'postgres.host' && \
    dasel put -f luigi.toml -t string -v '$PG_DBNAME' 'postgres.database' && \
    dasel put -f luigi.toml -t string -v '$PG_PORT' 'postgres.port' && \
    dasel put -f luigi.toml -t string -v '$PG_USER' 'postgres.user' && \
    dasel put -f luigi.toml -t string -v '$PGPASSWORD' 'postgres.password' && \
    dasel put -f luigi.toml -t string -v '$ONTOLOGY_PATH' 'ontology.path'

ENTRYPOINT ["python", "/chebi_dumps/dump_generation_process.py", "CreateDataProducts", "--local-scheduler"]