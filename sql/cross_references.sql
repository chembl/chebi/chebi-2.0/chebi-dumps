-- This query get the database references information used by the SDF files
WITH database_references AS (
    SELECT
    r.compound_id,
    string_agg(r.accession_number, ';') AS accession_number,
    CASE
        WHEN s.name = 'UniProt Entry' THEN 'UniProt Database Links'
        ELSE s.name || ' Database Links'
    END AS database_name
    FROM chebi_allstar.reference r
        INNER JOIN chebi_allstar.source s ON r.source_id = s.id
    GROUP BY
        r.compound_id,
        database_name
),
ontology_references AS (
    SELECT
        vdr.compound_id,
        string_agg(DISTINCT vdr.accession_number, ';') AS accession_number,
        CASE
            WHEN vdr.TYPE = 'REGISTRY_NUMBER' THEN vdr.source_name || ' Registry Numbers'
            WHEN vdr.TYPE = 'CAS' THEN 'CAS Registry Numbers'
            ELSE vdr.source_name || ' Database Links'
        END AS database_name
    FROM chebi_ontology.valid_database_references vdr
    GROUP BY
        vdr.compound_id,
        database_name
    ORDER BY vdr.compound_id
),
final_cte AS (
    SELECT
        compound_id,
        accession_number,
        database_name
    FROM database_references
    UNION
    SELECT
        compound_id,
        accession_number,
        database_name
    FROM ontology_references
)
SELECT * FROM final_cte;