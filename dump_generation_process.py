import itertools
import logging
import luigi
import polars as pl
import polars.selectors as cs
import time
import gzip

from datetime import datetime
from enum import StrEnum, auto
from luigi.contrib.external_program import ExternalProgramTask

from helper_functions.database_connection import get_pg_connection
from helper_functions.files_functions import (
    DUMPS_FOLDER,
    parent_folder,
    delete_all_checkpoints,
    TEMPLATES,
    SDF_FOLDER,
    FLAT_FILES_FOLDER,
    read_file,
    write_file,
)
from helper_functions.logger import logger
from helper_functions.chebi_allstar_dataframes import get_molfiles, get_cross_references
from helper_functions.ontology_functions import SDFVariants
from helper_functions.sdf_manager import SDFManager


class TableNames(StrEnum):
    """Tables name to export like SQL dump.
    Names here are the same in both schemas: chebi_allstars and chebi_flat_files
    """

    compounds = auto()
    chemical_data = auto()
    comments = auto()
    compound_origins = auto()
    database_accession = auto()
    names = auto()
    relation = auto()
    relation_type = auto()
    structures = auto()
    structure_registry = auto()
    wurcs = auto()
    source = auto()
    status = auto()
    reference = auto()


class FlatFiles(StrEnum):
    """Table names in chebi_flat_files schema ONLY.
    i.e., secondary_ids table does not exist in chebi_allstar schema, but it exists in chebi_flat_files ONLY."""

    secondary_ids = auto()


# Structure layers column data_type
Layers = pl.Struct(
    {
        "ESCAPE": pl.String,
        "FORMULA": pl.String,
        "SGROUP_DATA": pl.String,
        "TAUTOMER_HASH": pl.String,
        "CANONICAL_SMILES": pl.String,
        "NO_STEREO_SMILES": pl.String,
        "NO_STEREO_TAUTOMER_HASH": pl.String,
    }
)


class GeneratePostgresSQLDumps(ExternalProgramTask):
    release_date = luigi.DateParameter(default=datetime.now())
    release_version = luigi.IntParameter()

    def output(self):
        dumps = {
            # Schema and dumps.
            "postgres_schema": luigi.LocalTarget(DUMPS_FOLDER / "pgsql_create_tables.sql"),
            "postgres_dump": luigi.LocalTarget(DUMPS_FOLDER / "pgsql_allstars.dump"),
        }
        documentation = {
            "readme_dumps": luigi.LocalTarget(DUMPS_FOLDER / "README"),
            "install_dumps": luigi.LocalTarget(DUMPS_FOLDER / "INSTALL"),
            "license_dumps": luigi.LocalTarget(DUMPS_FOLDER / "LICENSE"),
            "schema_image": luigi.LocalTarget(DUMPS_FOLDER / f"chebi_{self.release_version}_schema.png"),
        }
        return dumps | documentation

    def program_args(self):
        # the first argument of this list is the path to the shell script,
        # The next arguments are the parameters used by the shell script.
        tables = [t for t in TableNames]
        return [f"{parent_folder / 'execute_dumps.sh'}", ";".join(tables)]

    def run(self):
        try:
            super().run()
            self.create_documentation()
        except Exception as e:
            logging.error(f"An uncontrolled exception has happened generating the PostgreSQL Dumps {e}")
            delete_all_checkpoints(SDF_FOLDER)

    def create_documentation(self):
        tables = [t for t in TableNames]
        release_date_format = self.release_date.strftime("%Y-%m-%d")

        schema_image = read_file(TEMPLATES / "chebi_schema.png", mode="rb")
        install_template = read_file(TEMPLATES / "instructions_import.tmpl")
        general_dumps_template = read_file(TEMPLATES / "general_dumps_template.tmpl")
        license = read_file(TEMPLATES / "LICENSE")

        # Replace placeholders in the templates
        install_template = install_template.replace("{{ release_version }}", str(self.release_version))
        install_template = install_template.replace("{{ release_date }}", release_date_format)

        general_dumps_template = general_dumps_template.replace("{{ core_tables }}", ", ".join(tables))
        general_dumps_template = general_dumps_template.replace("{{ release_version }}", str(self.release_version))
        general_dumps_template = general_dumps_template.replace("{{ release_date }}", release_date_format)

        # Write the output files for SQL dumps
        write_file(self.output()["readme_dumps"].path, general_dumps_template)
        write_file(self.output()["install_dumps"].path, install_template)
        write_file(self.output()["license_dumps"].path, license)
        write_file(self.output()["schema_image"].path, schema_image, mode="wb")


class GenerateSDF(luigi.Task):
    variants = luigi.EnumListParameter(enum=SDFVariants, default=[SDFVariants.FULL, SDFVariants.LITE])
    release_date = luigi.DateParameter(default=datetime.now())
    release_version = luigi.IntParameter()

    def output(self):
        chebi_sdf_3_stars = {
            f"chebi{variant}_3_stars": luigi.LocalTarget(SDF_FOLDER / f"chebi{variant}_3_stars.sdf.gz")
            for variant in self.variants
        }
        chebi_sdf = {
            f"chebi{variant}": luigi.LocalTarget(SDF_FOLDER / f"chebi{variant}.sdf.gz") for variant in self.variants
        }
        documentation = {
            "readme_sdf": luigi.LocalTarget(SDF_FOLDER / "README"),
            "license_sdf": luigi.LocalTarget(SDF_FOLDER / "LICENSE"),
        }
        return chebi_sdf | chebi_sdf_3_stars | documentation

    def run(self):
        try:
            molfiles = get_molfiles()
            cross_references = get_cross_references()
            for variant in self.variants:
                chebi_sdf_file_path = self.output().get(f"chebi{variant}").path
                chebi_sdf_3_stars_file_path = self.output().get(f"chebi{variant}_3_stars").path
                sdf_manager = SDFManager(variant, molfiles, cross_references)
                logger.info(f"> Generating sdf content for chebi{variant}")

                chebi_sdf_content, chebi_sdf_3_stars_content = sdf_manager.get_files_content()
                logger.info(f"> All content for chebi{variant} generated successfully")
                logger.info(f"> Creating sdf files for chebi{variant}")

                with (
                    gzip.open(chebi_sdf_file_path, mode="wb") as chebi_sdf,
                    gzip.open(chebi_sdf_3_stars_file_path, mode="wb") as chebi_sdf_3_stars,
                ):
                    chebi_sdf.write(str.encode(chebi_sdf_content))
                    logger.info(f"\tFile chebi{variant}.sdf.gz created")
                    chebi_sdf_3_stars.write(str.encode(chebi_sdf_3_stars_content))
                    logger.info(f"\tFile chebi{variant}_3_stars.sdf.gz created")

            self.create_documentation()
        except Exception as e:
            logging.error(
                f"An uncontrolled exception has happened generating the SDF files {e}, dropping all SDF files"
            )
            delete_all_checkpoints(SDF_FOLDER)

    def create_documentation(self):
        release_date_format = self.release_date.strftime("%Y-%m-%d")
        general_sdf_template = read_file(TEMPLATES / "general_sdf_template.tmpl")
        license = read_file(TEMPLATES / "LICENSE")
        general_sdf_template = general_sdf_template.replace("{{ release_version }}", str(self.release_version))
        general_sdf_template = general_sdf_template.replace("{{ release_date }}", release_date_format)
        # Write docs for SDF
        write_file(self.output()["readme_sdf"].path, general_sdf_template)
        write_file(self.output()["license_sdf"].path, license)


class GenerateFlatFiles(luigi.Task):
    """Class to generate the different flat files of ChEBI under the flat_files/ folder"""

    release_date = luigi.DateParameter(default=datetime.now())
    release_version = luigi.IntParameter()

    def output(self):
        flat_files = {
            f"{table_name}.tsv.gz": luigi.LocalTarget(FLAT_FILES_FOLDER / f"{table_name}.tsv.gz")
            for table_name in TableNames
        }
        documentation = {
            "readme_flat_files": luigi.LocalTarget(FLAT_FILES_FOLDER / "README"),
            "license_flat_files": luigi.LocalTarget(FLAT_FILES_FOLDER / "LICENSE"),
        }
        return flat_files | documentation

    def run(self):
        try:
            with get_pg_connection().cursor() as cursor:
                for table in itertools.chain(TableNames, FlatFiles):
                    logging.info(f"> Generating flat files for table {table}")
                    file_name = FLAT_FILES_FOLDER / f"{table}.tsv.gz"
                    with gzip.open(file_name, mode="wb") as file:
                        df = pl.read_database(
                            query=f"SELECT * FROM chebi_flat_files.{table}",
                            connection=cursor,
                            infer_schema_length=None,
                        )
                        # JSON columns are converted to structs by polars.
                        # Add more structs in df.select() method if necessary
                        if not (df_structs := df.select(cs.by_dtype(Layers))).is_empty():
                            for column_name in df_structs.columns:
                                df = df.with_columns(pl.col(column_name).struct.json_encode())

                        df.write_csv(file, separator="\t", float_scientific=True)

                self.create_documentation()
        except Exception as e:
            logging.error(
                f"An uncontrolled exception has happened generating the flat files {e}, dropping all flat files"
            )
            delete_all_checkpoints(FLAT_FILES_FOLDER)

    def create_documentation(self):
        release_date_format = self.release_date.strftime("%Y-%m-%d")
        general_flat_files_template = read_file(TEMPLATES / "general_flat_files.tmpl")
        license = read_file(TEMPLATES / "LICENSE")
        general_flat_files_template = general_flat_files_template.replace(
            "{{ release_version }}", str(self.release_version)
        )
        general_flat_files_template = general_flat_files_template.replace("{{ release_date }}", release_date_format)
        # Write docs for SDF
        write_file(self.output()["readme_flat_files"].path, general_flat_files_template)
        write_file(self.output()["license_flat_files"].path, license)


class CreateDataProducts(luigi.WrapperTask):
    variants = luigi.EnumListParameter(enum=SDFVariants, default=[SDFVariants.FULL, SDFVariants.LITE])
    release_version = luigi.IntParameter()
    release_date = luigi.DateParameter(default=datetime.now())

    def requires(self):
        yield GenerateSDF(variants=self.variants, release_date=self.release_date, release_version=self.release_version)
        yield GeneratePostgresSQLDumps(release_date=self.release_date, release_version=self.release_version)
        yield GenerateFlatFiles(release_date=self.release_date, release_version=self.release_version)


if __name__ == "__main__":
    start_time = time.time()
    luigi_run_result = luigi.run(detailed_summary=True)
    logger.info(f"Status: {luigi_run_result.status}")
    assert luigi_run_result.status in [
        luigi.execution_summary.LuigiStatusCode.SUCCESS,
        luigi.execution_summary.LuigiStatusCode.SUCCESS_WITH_RETRY,
    ]
    logger.info(f"> ETL process successfully finished, execution time: {time.time() - start_time}")
