# ChEBI Dumps

This repository has the code to generate all the ChEBI data products, except the ontology files. For the last mentioned,
there is another project called [ChEBI Ontology Generator](https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-ontology-generator).

## How to run this project

1. Clone the repository
2. Go to the local folder `cd chebi-dumps`
3. Create a new file called `luigi.toml`: `cp luigi.example.toml luigi.toml`
4. Set the PostgreSQL and Ontology variables in the above file.
5. `python dump_generation_process.py --release-version=1234 --workers=3 --log-level INFO`
6. Once the process finishes, you can see the generated data products in the different folders:
    * `generic_dumps:` All the SQL dumps
    * `SDF:` All the sdf files.

_All the content in the data products folders will be published on the FTP, except the README.md files_
